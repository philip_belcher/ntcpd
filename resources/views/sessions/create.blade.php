@extends('layouts.master')

@section('content')

    <div class="uk-width-1-1 uk-width-medium-1-2 uk-container-center">
       <div class="uk-panel uk-panel-box uk-panel-box-primary uk-width-auto uk-text-center">

           <h1 class="uk-panel-title">Login to your Account</h1>

           <form method="POST" action="/login" class="uk-form uk-form-stacked">

               {{ csrf_field() }}


               <div class="uk-margin">
                   <label class="uk-form-label" for="email">Email</label>
                   <div class="uk-form-controls">
                       <input class="uk-input" id="email" name="email" type="email">
                   </div>
               </div>

               <div class="uk-margin">
                   <label class="uk-form-label" for="password">Password</label>
                   <div class="uk-form-controls">
                       <input class="uk-input" id="password" name="password" type="password">
                   </div>
               </div>


               @include('partials.formerror')

               <button type="submit" class="uk-button uk-button-primary">Login</button>


           </form>

       </div>
    </div>

@endsection

