<form method="POST" action="/booking" class="uk-form uk-form-stacked uk-margin-bottom">

{{ csrf_field() }}


<div class="uk-grid">

    <div class="uk-width-1-1 uk-width-medium-1-2">

        <div class="uk-panel uk-panel-box uk-margin-bottom">

            <div class="uk-panel-body">

                <h3>Delegate Information</h3>

                @include('partials.formerror')

                <div id="delegate_container">

                    <div class="delegate">
                        <hr>
                        <div class="uk-margin">
                            <label class="uk-form-label">Delegate Name</label>
                            <div class="uk-form-controls">
                                <input class="uk-input uk-form-width-large" name="delegate[0][name]" type="text" value="{{ old('delegate_name') }}" required>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label">Delegate Email</label>
                            <div class="uk-form-controls">
                                <input class="uk-input uk-form-width-large" name="delegate[0][email]" type="text" value="{{ old('delegate_email') }}" required>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label">Delegate Requirements/Needs/Other Notes</label>
                            <div class="uk-form-controls">
                                <input class="uk-input uk-form-width-large" name="delegate[0][notes]" type="text" value="{{ old('delegate_notes') }}">
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" name="course_id" value="{{ $course->id ?? '' }}">

                <a href="#" class="uk-button uk-button-secondary" id="add_delegate_button"><i class="uk-icon-plus"></i> Add Delegate</a>

                <hr>

                <button type="submit" class="uk-button uk-button-primary">Book On This Course</button>

            </div>
        </div>
    </div>

</div>
</form>

@push('scripts')
<script src="/js/app.booking.js"></script>
@endpush