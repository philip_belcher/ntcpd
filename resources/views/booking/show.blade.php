@extends('layouts.master')


@section('content')

    <article class="uk-article">

        <h1 class="uk-article-title">Book This Course</h1>
        <h2>{{ $course->name }}</h2>

        <p><strong>Course Starts: </strong>{{ $course->start_date->toDayDateTimeString() }}</p>
        @if($course->end_date)
            <p class="uk-article-meta"><strong>Course Ends: </strong>{{ $course->end_date->toDayDateTimeString() }}</p>
        @endif


        <hr class="uk-article-divider">

        <h2>Fill in your details</h2>

        @include('booking.bookingform')

    </article>

@endsection