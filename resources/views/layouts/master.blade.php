<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NTCPD</title>

    <link rel="stylesheet" href="/css/uikit.min.css">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

</head>

<body>

    <div class="uk-container uk-container-center uk-margin-bottom">
        @include('partials.nav')
    </div>

    <div class="uk-container uk-container-center">

        @if($flash = session('message'))
            <div id="flash-message" class="uk-alert uk-alert-success" role="alert">
                {{ $flash }}
            </div>
        @endif

        @yield('content')

    </div>


    <script src="{{ mix('/js/app.js') }}"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/uikit.min.js"></script>

    @stack('scripts')

</body>

</html>