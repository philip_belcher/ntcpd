<nav class="uk-navbar">

        <ul class="uk-navbar-nav">
            <li><a href="/">Home</a></li>
            <li class="uk-parent" data-uk-dropdown aria-haspopup="true" aria-expanded="false">
                <a href="/courses">Courses</a>
                <div class="uk-dropdown uk-dropdown-navbar">
                    <ul class="uk-nav uk-nav-navbar">
                        <li><a href="/courses">View All Courses</a></li>
                        <li class="uk-nav-divider"></li>
                        @foreach($categories as $category)
                            <li><a href="/courses/{{ $category->slug }}">{{ $category->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </li>
        </ul>


    <div class="uk-navbar-flip">

        <ul class="uk-navbar-nav">

            @if(Auth::check())
                <li class="uk-parent" data-uk-dropdown aria-haspopup="true" aria-expanded="false">
                    <a>{{ Auth::user()->name }} <i class="uk-icon-caret-down"></i></a>
                    <div class="uk-dropdown uk-dropdown-navbar">
                        <ul class="uk-nav uk-navbar-dropdown-nav">
                            @if(Auth::user()->hasRole('admin'))
                                <li><a href="/admin">Administration</a></li>
                            @endif
                            <li><a href="/logout">Logout</a></li>
                        </ul>
                    </div>
                </li>
            @else
                <li><a href="/register">Register</a></li>
                <li><a href="/login">Login</a></li>
            @endif

        </ul>

    </div>
</nav>