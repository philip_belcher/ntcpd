@extends('admin.layouts.master')

@section('admin.content')

    <a class="uk-button uk-button-default" href="/admin/courses"><i class="fa fa-chevron-left" aria-hidden="true"></i> Cancel</a>

    <h2>Edit This Course</h2>

    @include('partials.formerror')

    <div class="uk-container uk-container-center">

        <form method="POST" action="/admin/courses" class="uk-form uk-form-stacked uk-margin-bottom">

            {{ csrf_field() }}

            {{ method_field('PATCH') }}

            <div class="uk-grid">

                <div class="uk-width-1-1 uk-width-medium-1-2">

                    <div class="uk-panel uk-panel-box uk-margin-bottom">
                        <h3 class="uk-panel-header">Course Information</h3>

                        <div class="uk-panel-body">

                            <div class="uk-margin">
                                <label class="uk-form-label" for="name">Name</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input uk-form-width-large" id="name" name="name" type="text" value="{{ $course->name }}" required>
                                </div>
                            </div>

                            <div class="uk-margin">
                                <label class="uk-form-label" for="slug">Slug</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input uk-form-width-large" id="slug" name="slug" type="text" value="{{ $course->slug }}" required>
                                </div>
                            </div>

                            <div class="uk-margin">
                                <label class="uk-form-label" for="published">Published</label>
                                <div class="uk-form-controls">
                                    <div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
                                        <label><input class="uk-radio" type="radio" name="published" value="1"> Yes</label>
                                        <label><input class="uk-radio" type="radio" name="published" checked value="0"> No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="uk-margin">
                                <label class="uk-form-label" for="archived">Archived</label>
                                <div class="uk-form-controls">
                                    <div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
                                        <label><input class="uk-radio" type="radio" name="archived" value="1"> Yes</label>
                                        <label><input class="uk-radio" type="radio" name="archived" checked value="0"> No</label>
                                    </div>
                                </div>
                            </div>

                            <div class="uk-grid">

                                <div class="uk-width-1-2">
                                    <div class="uk-margin">
                                        <label class="uk-form-label" for="start_day">Start Day</label>
                                        <div class="uk-form-controls">
                                            <input class="uk-input" id="start_day" name="start_day" type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" required>
                                        </div>
                                    </div>

                                    <div class="uk-margin">
                                        <label class="uk-form-label" for="start_time">Start Time</label>
                                        <div class="uk-form-controls">
                                            <input class="uk-input" id="start_time" name="start_time" type="text" data-uk-timepicker required>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-width-1-2">
                                    <div class="uk-margin">
                                        <label class="uk-form-label" for="end_day">End Day</label>
                                        <div class="uk-form-controls">
                                            <input class="uk-input" id="end_day" name="end_day" type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}">
                                        </div>
                                    </div>

                                    <div class="uk-margin">
                                        <label class="uk-form-label" for="end_time">End Time</label>
                                        <div class="uk-form-controls">
                                            <input class="uk-input" id="end_time" name="end_time" type="text" data-uk-timepicker>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="uk-margin">
                                <label class="uk-form-label" for="venue">Venue</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input uk-form-width-large" id="venue" name="venue" type="text">
                                </div>
                            </div>

                            <div class="uk-margin">
                                <span class="uk-form-label" for="categories">Categories</span>
                                <div class="uk-scrollable-box">
                                    <div class="uk-form-controls">
                                        @foreach($categories as $category)
                                            <input type="checkbox" id="cat_{{ $category->id }}"  name="categories[]" value="{{ $category->id }}"> <label for="cat_{{ $category->id }}">{{ $category->name }}</label><br>
                                        @endforeach
                                    </div>
                                </div>
                            </div>



                            <div class="uk-margin">
                                <label class="uk-form-label" for="description">Description</label>
                                <div class="uk-form-controls">
                                    <textarea class="form-control uk-textarea uk-form-width-large" rows="10" id="description" name="description"></textarea>
                                </div>
                            </div>

                            <div class="uk-margin">
                                <label class="uk-form-label" for="image">Image</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input uk-form-width-large" id="image" name="image" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-1-1 uk-width-medium-1-2">
                    <div class="uk-panel uk-panel-box uk-margin-bottom">
                        <h3 class="uk-panel-header">Booking Information</h3>

                        <div class="uk-panel-body">

                            <div class="uk-margin">
                                <span class="uk-form-label" for="facilitator">Facilitator</span>
                                <div class="uk-scrollable-box">
                                    <div class="uk-form-controls">
                                        @if($facilitators)
                                            @foreach($facilitators as $facilitator)
                                                <input type="checkbox" id="facilitator_{{ $facilitator->id }}"  name="facilitator[]" value="{{ $facilitator->id }}"> <label for="facilitator_{{ $facilitator->id }}_{{ $facilitator->id }}">{{ $facilitator->name }} - {{ $facilitator->email }}</label><br>
                                            @endforeach
                                        @else
                                            <p>Can't find any Facilitator Users!</p>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="uk-margin">
                                <label class="uk-form-label" for="capacity">Capacity</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input uk-form-width-large" id="capacity" name="capacity" type="text">
                                </div>
                            </div>

                            <div class="uk-margin">
                                <label class="uk-form-label" for="book_online">Allow Online Bookings?</label>
                                <div class="uk-form-controls">
                                    <div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
                                        <label><input class="uk-radio" type="radio" name="book_online" checked value="1"> Yes</label>
                                        <label><input class="uk-radio" type="radio" name="book_online" value="0"> No</label>
                                    </div>
                                </div>
                            </div>


                            <div class="uk-margin">
                                <label class="uk-form-label" for="external_booking_url">External Booking Link</label>
                                <div class="uk-form-controls">
                                    <input class="uk-input uk-form-width-large" id="external_booking_url" name="external_booking_url" type="text">
                                </div>
                            </div>

                            <button type="submit" class="uk-button uk-button-primary">Add Course</button>

                        </div>
                    </div>
                </div>







        </form>

    </div>
    </div>

    </div>

@endsection

@push('scripts')
<script src="/js/app.admin.courses.js"></script>
@endpush