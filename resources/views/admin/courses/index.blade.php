@extends('admin.layouts.master')

@section('admin.content')

    <h2>All Courses</h2>

    <a class="uk-button uk-button-primary" href="/admin/courses/create"><i class="uk-icon-plus" aria-hidden="true"></i> Add New Course</a>

    <table class="uk-table uk-table-hover">
        <thead>
        <tr>
            <th class="uk-text-uppercase uk-width-1-3">Course Name</th>
            <th class="uk-text-uppercase uk-text-center">Published</th>
            <th class="uk-text-uppercase uk-text-nowrap">Course Start Date</th>
            <th class="uk-text-uppercase uk-text-nowrap">Modified Date</th>
        </tr>
        </thead>
        <tfoot>
            <tr>
                <td colspan="4">
                    {{ $courses->links() }}
                </td>
            </tr>
        </tfoot>
        <tbody>

        @foreach($courses as $course)
            <tr>
                <td class="uk-table-link"><a href="/admin/courses/{{ $course->slug }}" >{{ $course->name }}</a></td>
                <td class="uk-text-center">
                    @if($course->published)
                        <i class="uk-icon-check-circle admin-tick"></i>
                    @else
                        <i class="uk-icon-times-circle admin-cross"></i>
                    @endif
                </td>
                <td>
                    {{ $course->start_date->toDayDateTimeString() }}</td>
                <td>{{ $course->updated_at->toDayDateTimeString() }}</td>
            </tr>
        @endforeach

        </tbody>
    </table>

@endsection