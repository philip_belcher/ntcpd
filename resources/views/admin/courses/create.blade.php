@extends('admin.layouts.master')

@section('admin.content')

    <a class="uk-button uk-button-default" href="/admin/courses"><i class="fa fa-chevron-left" aria-hidden="true"></i> Cancel</a>

    <h2>Add/Edit A Course</h2>

    @include('partials.formerror')

    <div class="uk-container uk-container-center">

        @if(isset($course))

            <form method="POST" action="/admin/courses" class="uk-form uk-form-stacked uk-margin-bottom">
                {{ method_field('PATCH') }}
        @else
            <form method="POST" action="/admin/courses" class="uk-form uk-form-stacked uk-margin-bottom">
        @endif

            @include('admin.courses.formbody')

         </form>

    </div>


@endsection

@push('scripts')
    <script src="/js/app.admin.courses.js"></script>
@endpush