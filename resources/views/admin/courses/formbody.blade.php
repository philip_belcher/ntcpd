{{ csrf_field() }}

@php

    //If it's an existing course we'll need to parse some of the data to fit into the fields (i.e. the start_date values)
    if(isset($course))
    {

        //Sort out dates
        $start_day = $course->start_date->toDateString();
        $start_time = $course->start_date->toTimeString();

        if($course->end_date) {
            $end_day = $course->end_date->toDateString();
            $end_time = $course->end_date->toTimeString();
        }

        $existing_categories = $course->category;
        $existing_facilitators = $course->facilitators;



    }




@endphp

<div class="uk-grid">

    <div class="uk-width-1-1 uk-width-medium-1-2">

        <div class="uk-panel uk-panel-box uk-margin-bottom">
            <h3 class="uk-panel-header">Course Information</h3>

            <div class="uk-panel-body">

                <input type="hidden" name="course_id" value="{{ $course->id ?? '' }}">

                <div class="uk-margin">
                    <label class="uk-form-label" for="name">Name</label>
                    <div class="uk-form-controls">
                        <input class="uk-input uk-form-width-large" id="name" name="name" type="text" value="{{ old('name', $course->name ?? '') }}" required>
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="slug">Slug</label>
                    <div class="uk-form-controls">
                        <input class="uk-input uk-form-width-large" id="slug" name="slug" type="text" value="{{ old('slug', $course->slug ?? '') }}" required>
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="published">Published</label>
                    <div class="uk-form-controls">
                        <div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
                            <label><input class="uk-radio" type="radio" name="published"
                                          @if( (old('published') === 1) || (isset($course) && ($course->published === 1)) )
                                            checked="checked"
                                          @endif
                                          value="1"> Yes</label>
                            <label><input class="uk-radio" type="radio" name="published"
                                          @if( (old('published') === 0) ||  (isset($course) && ($course->published === 0) || (!isset($course))) )
                                            checked="checked"
                                          @endif
                                          value="0"> No</label>
                        </div>
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="archived">Archived</label>
                    <div class="uk-form-controls">
                        <div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
                            <label><input class="uk-radio" type="radio" name="archived"
                                          @if( (old('archived') === 1) || (isset($course) && ($course->archived === 1)) )
                                          checked="checked"
                                          @endif
                                          value="1"> Yes</label>
                            <label><input class="uk-radio" type="radio" name="archived"
                                          @if( (old('archived') === 0) ||  (isset($course) && ($course->archived === 0) || (!isset($course))) )
                                          checked="checked"
                                          @endif
                                          value="0"> No</label>
                        </div>
                    </div>
                </div>

                <div class="uk-grid">

                    <div class="uk-width-1-2">
                        <div class="uk-margin">
                            <label class="uk-form-label" for="start_day">Start Day</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="start_day" name="start_day" type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" value="{{ old('start_date', $start_day ?? '') }}" required>
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="start_time">Start Time</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="start_time" name="start_time" type="text" data-uk-timepicker value="{{ old('start_date', $start_time ?? '') }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="uk-width-1-2">
                        <div class="uk-margin">
                            <label class="uk-form-label" for="end_day">End Day</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="end_day" name="end_day" type="text" data-uk-datepicker="{format:'YYYY-MM-DD'}" value="{{ old('end_date', $end_day ?? '') }}" >
                            </div>
                        </div>

                        <div class="uk-margin">
                            <label class="uk-form-label" for="end_time">End Time</label>
                            <div class="uk-form-controls">
                                <input class="uk-input" id="end_time" name="end_time" type="text" data-uk-timepicker  value="{{ old('end_date', $end_time ?? '') }}" >
                            </div>
                        </div>
                    </div>

                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="venue">Venue</label>
                    <div class="uk-form-controls">
                        <input class="uk-input uk-form-width-large" id="venue" name="venue" type="text"  value="{{ old('venue', $course->venue ?? '') }}">
                    </div>
                </div>

                <div class="uk-margin">
                    <span class="uk-form-label" for="categories">Categories</span>
                    <div class="uk-scrollable-box">
                        <div class="uk-form-controls">
                            @foreach($categories as $category)
                                <input type="checkbox" id="cat_{{ $category->id }}"  name="categories[]" value="{{ $category->id }}"
                                    @if(isset($course))
                                       @foreach($existing_categories as $existing_category)
                                            @if($existing_category->id === $category->id)
                                                checked="checked"
                                            @endif
                                       @endforeach
                                    @endif>
                                <label for="cat_{{ $category->id }}">{{ $category->name }}</label><br>
                            @endforeach
                        </div>
                    </div>
                </div>



                <div class="uk-margin">
                    <label class="uk-form-label" for="description">Description</label>
                    <div class="uk-form-controls">
                        <textarea class="form-control uk-textarea uk-form-width-large" rows="10" id="description" name="description" >{{ old('description', $course->description ?? '') }}</textarea>
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="image">Image</label>
                    <div class="uk-form-controls">
                        <input class="uk-input uk-form-width-large" id="image" name="image" type="text"  value="{{ old('image', $course->image ?? '') }}">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-medium-1-2">
        <div class="uk-panel uk-panel-box uk-margin-bottom">
            <h3 class="uk-panel-header">Booking Information</h3>

            <div class="uk-panel-body">

                <div class="uk-margin">
                    <span class="uk-form-label" for="facilitator">Facilitator</span>
                    <div class="uk-scrollable-box">
                        <div class="uk-form-controls">
                            @if($facilitators)
                                @foreach($facilitators as $facilitator)
                                    <input type="checkbox" id="facilitator_{{ $facilitator->id }}"  name="facilitator[]" value="{{ $facilitator->id }}"
                                        @if(isset($course))
                                           @foreach($existing_facilitators as $existing_facilitator)
                                                @if($facilitator->id === $existing_facilitator->id)
                                                    checked="checked"
                                                @endif
                                           @endforeach
                                        @endif >
                                    <label for="facilitator_{{ $facilitator->id }}_{{ $facilitator->id }}">{{ $facilitator->name }} - {{ $facilitator->email }}</label><br>
                                @endforeach
                            @else
                                <p>Can't find any Facilitator Users!</p>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="capacity">Capacity</label>
                    <div class="uk-form-controls">
                        <input class="uk-input uk-form-width-large" id="capacity" name="capacity" type="text"  value="{{ old('capacity', $course->capacity ?? '') }}">
                    </div>
                </div>

                <div class="uk-margin">
                    <label class="uk-form-label" for="book_online">Allow Online Bookings?</label>
                    <div class="uk-form-controls">
                        <div class="uk-margin uk-grid-small uk-child-width-auto" uk-grid>
                            <label><input class="uk-radio" type="radio" name="book_online"
                                          @if( (old('book_online') === 1) || (isset($course) && ($course->book_online === 1) || (!isset($course))) )
                                          checked="checked"
                                          @endif
                                          value="1"> Yes</label>
                            <label><input class="uk-radio" type="radio" name="book_online"
                                          @if( (old('book_online') === 0) ||  (isset($course) && ($course->book_online === 0)) )
                                          checked="checked"
                                          @endif
                                          value="0"> No</label>
                        </div>
                    </div>
                </div>


                <div class="uk-margin">
                    <label class="uk-form-label" for="external_booking_url">External Booking Link</label>
                    <div class="uk-form-controls">
                        <input class="uk-input uk-form-width-large" id="external_booking_url" name="external_booking_url" type="text"  value="{{ old('external_booking_link', $course->external_booking_link ?? '') }}">
                    </div>
                </div>

                <button type="submit" class="uk-button uk-button-primary">{{ isset($course) ? "Update" : "Add" }} Course</button>

            </div>
        </div>
    </div>
