<nav class="uk-navbar uk-contrast admin-navbar">

    <div class="uk-navbar-">

        <ul class="uk-navbar-nav">
            <li><a href="/admin"><i class="uk-icon-home"></i> Admin Home</a></li>
        </ul>

    </div>

    <div class="uk-navbar-flip">

        <ul class="uk-navbar-nav">
           <li><a href="/" target="_blank"><i class="uk-icon-external-link"></i> Front-End</a></li>
           <li><a href="/logout"><i class="uk-icon-sign-out"></i> Logout</a></li>
        </ul>

    </div>
</nav>