@extends('admin.layouts.master')

@section('admin.content')

    <h2>Dashboard</h2>

    <div class="uk-grid uk-text-center" data-uk-grid-match="{target:'.uk-panel'}">
        <div class="uk-width-1-1 uk-width-medium-1-3">
             <div class="uk-panel uk-panel-box uk-panel-body uk-panel-hover uk-margin-bottom">
                    <h3 class="uk-panel-title">Course Manager</h3>
                    <p>View and create new Courses</p>
                    <p><a href="/admin/courses" class="uk-button uk-button-primary">Course Manager</a></p>
             </div>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-3">
            <div class="uk-panel uk-panel-box uk-panel-body uk-panel-hover uk-margin-bottom">
                    <h3 class="uk-panel-title">Category Manager</h3>
                    <p>View and create new Categories for Courses</p>
                    <p><a href="/admin/categories" class="uk-button uk-button-primary">Category Manager</a></p>
            </div>
        </div>
        <div class="uk-width-1-1 uk-width-medium-1-3">
            <div class="uk-panel uk-panel-box uk-panel-body uk-panel-hover uk-margin-bottom">
                    <h3 class="uk-panel-title">Bookings</h3>
                    <p>View bookings for all Courses</p>
                    <p><a href="/admin/bookings" class="uk-button uk-button-primary">Bookings Manager</a></p>
            </div>
        </div>
    </div>

@endsection