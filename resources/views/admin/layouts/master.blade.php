<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NTCPD Administration</title>

    <link rel="stylesheet" href="/css/uikit.min.css">
    <link rel="stylesheet" href="/css/components/datepicker.min.css">
    <link rel="stylesheet" href="/css/components/autocomplete.min.css">
    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

</head>

<body>

<div class="uk-container uk-container-center uk-margin-bottom">
    @include('admin.partials.nav')
</div>

<div class="uk-container uk-container-center">

    @if($flash = session('message'))
        <div id="flash-message" class="uk-alert uk-alert-success" role="alert">
            {{ $flash }}
        </div>
    @endif

    @yield('admin.content')

</div>




<script src="/js/jquery.min.js"></script>
<script src="/js/uikit.min.js"></script>
<script src="/js/components/datepicker.min.js"></script>
<script src="/js/components/autocomplete.min.js"></script>
<script src="/js/components/timepicker.min.js"></script>

@stack('scripts')

</body>

</html>