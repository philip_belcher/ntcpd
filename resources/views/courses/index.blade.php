@extends('layouts.master')


@section('content')

    <h2>Upcoming Courses</h2>

    <div class="uk-grid" data-uk-grid-match="{target:'.uk-panel'}">

    @foreach($courses as $course)
        <div class="uk-width-1-1 uk-width-medium-1-3">
            <div class="uk-panel uk-panel-box uk-margin-bottom">
                <div class="uk-panel-teaser">
                    <a href="/course/{{ $course->slug }}"><img src="{{ $course->image }}" ></a>
                </div>
                <div class="uk-panel-header">
                    <h3 class="uk-card-title uk-margin-remove-bottom">{{ $course->name }}</h3>
                      <p class="uk-text-meta uk-margin-remove-top">{{ $course->start_date->toDayDateTimeString() }}</p>
                </div>
                <div class="uk-panel-body">

                    @foreach($course->category as $category)
                        <a href="/courses/{{ $category->slug }}" class="uk-badge">
                            {{ $category->name }}
                        </a>
                    @endforeach

                    <p>{{ $course->description }}</p>

                    <a href="/course/{{ $course->slug }}" class="uk-button uk-button-primary uk-width-1-1">view course</a>
                </div>


            </div>
        </div>

    @endforeach

    </div>

    {{ $courses->links() }}

@endsection