@extends('layouts.master')


@section('content')

    <article class="uk-article">

        <h1 class="uk-article-title">{{ $course->name }}</h1>

        <p class="uk-article-meta"><strong>Course Starts: </strong>{{ $course->start_date->toDayDateTimeString() }}</p>
        @if($course->end_date)
            <p class="uk-article-meta"><strong>Course Ends: </strong>{{ $course->end_date->toDayDateTimeString() }}</p>
        @endif

        <p>
            <img src="{{ $course->image }}" class="uk-align-right" >
            {{ $course->description }}.
        </p>

        @if( count($course->facilitators))
            <h4>Facilitator</h4>
            @foreach($course->facilitators as $facilitator)
                <li>{{ $facilitator->name }}</li>
            @endforeach
        @endif

        <hr class="uk-article-divider">

        @if(Auth::check())
            <a class="uk-button uk-button-primary" href="/course/{{ $course->slug }}/booking">Book this course</a>
        @else
            <a href="/login" class="uk-button">You must be logged in to book on to a course</a>
        @endif


    </article>

@endsection