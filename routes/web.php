<?php

Route::get('/', 'CourseController@index')->name('home');


Route::get('/courses', 'CourseController@index');
Route::get('/course/{course}', 'CourseController@show');

Route::get('/courses/{category}', 'CategoryController@index');

Route::get('/course/{course}/booking', 'BookingController@show')->middleware('auth');
Route::post('/booking', 'BookingController@store')->middleware('auth');


Route::get('/register', 'RegistrationController@create');
Route::post('/register', 'RegistrationController@store');

Route::get('/login', 'SessionsController@create');
Route::post('/login', 'SessionsController@store');
Route::get('/logout', 'SessionsController@destroy');



Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth','admin:admin,owner']], function () {

    Route::get('/','AdminController@index')->name('admin.home');

    Route::get('/courses', 'CourseController@index')->name('admin.courses');
    Route::get('/courses/create', 'CourseController@create');
    Route::post('/courses', 'CourseController@store');
    Route::patch('/courses', 'CourseController@update');
    Route::get('/courses/{course}', 'CourseController@show');


});