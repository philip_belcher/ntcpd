(function(ntcpdbooking) {

    // The global jQuery object is passed as a parameter
    ntcpdbooking(window.jQuery, window, document);

}(function($, window, document) {

    // The $ is now locally scoped

    //Set up references for later
    var delegate_container = $('#delegate_container');

    var add_delegate_button = $('#add_delegate_button');

    var delegate = $(".delegate:first").clone().append('<a href="#" class="uk-button uk-margin-bottom remove_delegate_button"><i class="uk-icon-times"></i> Remove Delegate</a>');

    var delegate_count = 0;



    // Listen for the jQuery ready event on the document
    $(function() {

        //When the add delegate button is clicked
        add_delegate_button.on("click", add_delegate);

    });

    //Add a new delegate field group
    function add_delegate(e)
    {
        //Prevent default clicking
        e.preventDefault();

        //Increase delegate count value so we cna use it in the array value for the inputs
        delegate_count++;

        //Clone the delegate html
        var new_delegate = delegate.clone();
        //Find all inputs within the new delegate clone
        new_delegate.find('input')
            .each(function(){
                //Do a regex for the square brackets and replace it with the new array count value
                this.name = this.name.replace(/\[(\d+)\]/,'['+ delegate_count +']');
            })
            .end()
            .appendTo(delegate_container);

        //register the new remove delegate button with a remove_delegate function
        $('.remove_delegate_button').on("click", remove_delegate);

    }

    //Removes the attached delegate field group
    function remove_delegate(e)
    {
        e.preventDefault();
        $(this).parent('div').remove();
        if (delegate_count >= 1) delegate_count--;

    }




}));