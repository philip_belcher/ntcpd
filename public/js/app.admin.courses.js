(function(ntcpdadmincourses) {

    // The global jQuery object is passed as a parameter
    ntcpdadmincourses(window.jQuery, window, document);

}(function($, window, document) {

    // The $ is now locally scoped

    var course_name = $('#name');

    var slug_name = $('#slug');

    // Listen for the jQuery ready event on the document
    $(function() {

        auto_slug();

    });

    function auto_slug()
    {
        //When the name textfield loses focus AND has some text in it
        course_name.focusout(function() {
           if(coursename = $(this).val()) {
               //then set the slug field text to a sluggified version
               slug_name.val(convertToSlug(coursename));
           }
        });

    }

    function convertToSlug(Text)
    {
        return Text
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-');
    }


}));