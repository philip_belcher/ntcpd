<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * Creates a specific admin account and 10 other users from a factory
     *
     * Then adds the default Roles to the Roles table
     *
     * Then sets some of the users as Facilitator Role so they show up in the facilitator list
     *
     * @return void
     */
    public function run()
    {
        //Create my admin account
        DB::table('users')->insert([
            'name' => 'Philip Belcher',
            'email' => 'mrsmeeth@gmail.com',
            'password' => bcrypt('password'),
            'remember_token' => str_random(10)
        ]);

        //Create default user Roles
        DB::table('roles')->insert([
            ['name' => 'admin'],
            ['name' => 'owner'],
            ['name' => 'facilitator']
        ]);

        //Set up admin role on admin account
        DB::table('role_user')->insert([
            ['role_id' => 1, 'user_id' => 1],
            ['role_id' => 2, 'user_id' => 1],
            ['role_id' => 3, 'user_id' => 1]
        ]);


        //Create some users as facilitators
        factory(App\User::class, 4)->create()->each(function($u) {
            $u->roles()->save(App\Role::where('name', 'facilitator')->first());
        });






    }
}
