<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->dateTime('start_date');
            $table->dateTime('end_date')->nullable();
            $table->string('venue')->nullable();
            $table->integer('capacity')->unsigned()->nullable();
            $table->boolean('published');
            $table->boolean('archived');
            $table->boolean('book_online')->nullable();
            $table->string('external_booking_url')->nullable();
            $table->timestamps();
        });

        Schema::create('course_facilitator', function (Blueprint $table) {
            $table->integer('course_id');
            $table->integer('facilitator_id');
            $table->primary(['course_id', 'facilitator_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
        Schema::dropIfExists('course_facilitator');
    }
}
