<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Course::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->sentence,
        'slug' => $faker->unique()->slug,
        'description' => $faker->paragraph,
        'image' => $faker->imageUrl,
        'start_date' => $faker->dateTimeBetween('+0 days', '+2 years'),
        'venue' => $faker->word,
        'capacity' => 30,
        'published' => 1,
        'archived' => 0,
        'book_online' => 1

    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    $word = $faker->unique()->word;
    return [
        'name' => ucfirst($word),
        'slug' => $word
    ];
});
