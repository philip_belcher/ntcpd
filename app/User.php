<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Returns an Eloquent Collection of all the Roles a User belongs to
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }


    /**
     * Checks the User for a Role that is passed in
     *
     * @param string $name
     * @return boolean
     */
    public function hasRole($name)
    {

        foreach ($this->roles as $role)
        {
            if($role->name == $name) return true;
        }

        return false;

    }


    /**
     * Assign a User a Role
     *
     */
    public function assignRole($role)
    {
        return $this->roles()->attach($role);
    }


    /**
     * Remove a Role from a User
     *
     */
    public function revokeRole($role)
    {
        return $this->roles()->detach($role);
    }


    /**
     * Returns an Eloquent Collection of all the Courses a User Facilitates
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function facilitates()
    {
        return $this->belongsToMany('App\Course', 'course_facilitator', 'facilitator_id', 'course_id');
    }


    public function bookings()
    {
        return $this->hasMany('App\Bookings');
    }

}
