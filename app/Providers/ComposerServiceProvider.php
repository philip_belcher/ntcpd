<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Provide All Categories to the relevant admin views
        view()->composer(['admin.courses.create','admin.courses.show'], function($view){

            $categories = \App\Category::all();

            //Get all Facilitator Users
            $facilitators = \App\User::whereHas('roles', function ($query) {
                $query->where('name', '=', 'facilitator');
            })->get();


            $view->with(compact('categories', 'facilitators'));

        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
