<?php

namespace App\Providers;

use App\Role;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        //Send the navigation partial the category data so it can generate the menu
        view()->composer('partials.nav', function($view) {

            $categories = \App\Category::all();

            $view->with(compact('categories'));

        });


        //When a User is created, automatically assign it to the Registered Role by default
        User::created(function ($user) {

            $user->assignRole(Role::whereName('registered')->get());

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
