<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{

    protected $guarded = [];

    //Cast the delegates column as an array
    protected $casts = [
        'delegates' => 'array',
    ];
}
