<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    /**
     * Returns an Eloquent Collection of all the Users belong to
     *
     * @returns Illuminate\Database\Eloquent\Collection
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }


}
