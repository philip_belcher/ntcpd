<?php

namespace App\Http\Requests;

use App\Course;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CourseForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name' => 'required|max:255',
                    'slug' => 'required|unique:courses|max:255',
                    'start_day' => 'required',
                    'start_time' => 'required',
                    'capacity' => 'numeric|nullable'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name' => 'required|max:255',
                    'slug' => 'required|max:255|unique:courses,slug,'.$this->course_id,
                    'start_day' => 'required',
                    'start_time' => 'required',
                    'capacity' => 'numeric|nullable'


                ];
            }
            default:break;
        }

    }


    /**
     * Create a Course and save it
     */
    public function persist()
    {
        //Pretty gross at the moment but I've had to join the date and time fields together as
        //a new Carbon instance as the Date and Time fields are separate on the form
        $start_date = new Carbon(request('start_day') . request('start_time'));
        $end_date = request('end_day') ? new Carbon(request('end_day') . request('end_time')) : null;

        $course = Course::create([
            'name' => request('name'),
            'slug' => request('slug'),
            'description' => request('description'),
            'image' => request('image'),
            'start_date' => $start_date,
            'end_date' => $end_date,
            'venue' => request('venue'),
            'capacity' => request('capacity'),
            'published' => request('published'),
            'archived' => request('archived'),
            'book_online' => request('book_online'),
            'external_booking_url' => request('external_booking_url'),

        ]);

        //If the user has selected any Category for the course, then attach it to the newly created course and save
        if ($categories = request('categories'))
        {
            $course->category()->attach($categories);
            $course->save();

        }

        //If the user has selected any Facilitators for the course, then attach it to the newly created course and save
        if ($facilitators = request('facilitator'))
        {
            $course->facilitators()->attach($facilitators);
            $course->save();

        }


    }

    public function update()
    {
        $course = Course::findOrFail(request('course_id'));

        //Pretty gross at the moment but I've had to join the date and time fields together as
        //a new Carbon instance as the Date and Time fields are separate on the form
        $start_date = new Carbon(request('start_day') . request('start_time'));
        $end_date = request('end_day') ? new Carbon(request('end_day') . request('end_time')) : null;

        $course->update([
            'name' => request('name'),
            'slug' => request('slug'),
            'description' => request('description'),
            'image' => request('image'),
            'start_date' => $start_date,
            'end_date' => $end_date,
            'venue' => request('venue'),
            'capacity' => request('capacity'),
            'published' => request('published'),
            'archived' => request('archived'),
            'book_online' => request('book_online'),
            'external_booking_url' => request('external_booking_url'),

        ]);


        if($categories = request('categories'))
        {
            $course->category()->sync($categories);
            $course->save();

        }
        else {
            //All categories were unselected so detach all
            $course->category()->detach();
            $course->save();
        }

        if ($facilitators = request('facilitator'))
        {
            $course->facilitators()->sync($facilitators);
            $course->save();

        }
        else {
            //All facilitators were unselected so detach all
            $course->facilitators()->detach();
            $course->save();
        }


    }
}
