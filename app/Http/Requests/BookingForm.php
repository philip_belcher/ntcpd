<?php

namespace App\Http\Requests;

use App\Booking;
use Illuminate\Foundation\Http\FormRequest;

class BookingForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'delegate.*.name' => 'required|max:255',
            'delegate.*.email' => 'required|email|max:255'
        ];

    }

    public function persist()
    {


        Booking::create([
            'user_id' => $this->user()->id,
            'course_id' => request('course_id'),
            'delegates' =>
                request('delegate')

        ]);
    }
}
