<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {

        //Check that the currently logged in user has an admin role
        //If not, flash a message and redirect home
        if (! $request->user()->hasRole($role)) {

            session()->flash('message', 'You are not authorised to do that');

            return redirect()->route('home');

        }

        return $next($request);
    }
}
