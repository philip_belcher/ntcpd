<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionsController extends Controller
{

    /**
     * Only allow Guests to access this controller, except the destroy method
     * - obviously - as logged in users need to be able to log out
     */
    public function __construct()
    {
        $this->middleware('guest')->except('destroy');
    }


    /**
     * Displays the login form
     */
    public function create()
    {
        return view('sessions.create');
    }


    /**
     * Attempt a User login
     */
    public function store()
    {

        // Attempt to login
        // If Fail, return back with error message
        if (! auth()->attempt(request(['email', 'password']))) {
            return back()->withErrors([
                'message' => 'It looks like you have entered your details incorrectly or you do not have an account'
            ]);
        }

        session()->flash('message', 'You are now logged in');

        return redirect()->route('home');

    }

    /**
     * Logout a User and return home
     */
    public function destroy()
    {

        auth()->logout();

        session()->flash('message', 'You are now logged out');

        return redirect()->route('home');

    }


}
