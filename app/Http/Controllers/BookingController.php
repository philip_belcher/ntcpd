<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Requests\BookingForm;

class BookingController extends Controller
{

    public function show(Course $course)
    {
        return view('booking.show', compact('course'));
    }

    public function store(BookingForm $form)
    {
        $form->persist();

        session()->flash('message', 'You have booked onto this course');

        return redirect()->route('home');
    }

}
