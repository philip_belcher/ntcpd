<?php

namespace App\Http\Controllers;

use App\Category;
use App\Course;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index(Category $category)
    {

        $courses = $category->courses()->upcoming()->unarchived()->published()->paginate(config('ntcpd.num_of_items', 10));

        return view('courses.index', compact('courses'));

    }

}
