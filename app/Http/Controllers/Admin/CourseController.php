<?php

namespace App\Http\Controllers\Admin;

use App\Course;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseForm;

class CourseController extends Controller
{

    public function index()
    {
        $courses = Course::orderBy('updated_at', 'desc')->paginate(15);

        return view('admin.courses.index', compact('courses'));

    }


    public function show(Course $course)
    {
        return view('admin.courses.create', compact('course'));
    }


    public function create()
    {
        return view('admin.courses.create');
    }

    public function store(CourseForm $form)
    {

        $form->persist();

        session()->flash('message', 'Your Course Has Been Created');

        return redirect()->route('admin.courses');

    }


    public function update(CourseForm $form)
    {

        $form->update();

        session()->flash('message', 'Your Course Has Been Updated');

        return redirect()->route('admin.courses');

    }





}
