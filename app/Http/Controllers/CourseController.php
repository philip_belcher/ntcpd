<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{

    public function index()
    {

        $courses = Course::upcoming()->unarchived()->published()->paginate(config('ntcpd.num_of_items', 10));

        return view('courses.index', compact('courses'));

    }


    public function show(Course $course)
    {
        return view('courses.show', compact('course'));
    }






}
