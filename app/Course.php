<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'start_date',
        'end_date'
    ];

    public function category()
    {
        return $this->belongsToMany(Category::class);
    }


    public function scopeUpcoming($query)
    {
        return $query->where('start_date', '>=', Carbon::now())
            ->orderBy('start_date', 'asc');
    }

    public function scopePublished($query)
    {
        return $query->where('published', 1);
    }

    public function scopeUnarchived($query)
    {
        return $query->where('archived', 0);
    }

    public function scopeArchived($query)
    {
        return $query->where('archived', 1);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Returns an Eloquent Collection of all the Facilitators that belong to this Course
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function facilitators()
    {
        return $this->belongsToMany('App\User', 'course_facilitator', 'course_id', 'facilitator_id');
    }


    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

}
